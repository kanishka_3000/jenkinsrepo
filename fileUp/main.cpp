#include <iostream>
#include <list>
#include <app/input/InputProcessor.h>
#include <app/core/impl/FactoryImpl.h>
#include <app/core/Application.h>
using namespace std;
void printWelcomeMessage()
{

}

int main(int argc, char *argv[])
{

    app::input::InputProcessor inputProcessor(argc, argv);

    int cacheSize = -1;
    string readerFile;
    string writerFile;
    string itemFile;
    std::list<std::string> errors;
    bool validated = inputProcessor.getInput(cacheSize, readerFile,
                                             writerFile, itemFile, errors);
    if(validated == false)
    {
        cout << "SETUP ERROR!!!" << endl;
        for(string err : errors)
        {
            cout << err.c_str() << endl;
        }
        return 0;
    }
    int mode = inputProcessor.getMode();
    app::core::MODE md;
    if(mode == 1)
    {
        md = app::core::MODE::CONCURRENT;
        cout << "Running in  concurrent mode" << endl;
    }
    else
    {
        md = app::core::MODE::READERS_AFTER_WRITERS;
        cout << "Running in  reader after writers mode" << endl;
    }
    app::core::FactoryImpl factory;
    app::core::Application application(factory);
    application.init(cacheSize, readerFile, writerFile,
                     itemFile, md);

    return 0;
}
