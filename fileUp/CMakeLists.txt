cmake_minimum_required (VERSION 2.6)
set (CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -pthread")
project (CacheProj C CXX)
find_package (Threads)
include_directories(".")
add_executable(cache 
    app/io/impl/FileReader.cpp 
    main.cpp 
    app/io/impl/InputData.cpp 
    app/io/impl/OutputData.cpp 
    app/io/impl/FileWriter.cpp 
    app/io/impl/ReaderDumpInput.cpp 
    app/core/impl/DataManagerImpl.cpp 
    app/reader/impl/ReaderImpl.cpp 
    app/writer/impl/WriterImpl.cpp 
    app/io/impl/ItemDumpInput.cpp 
    app/core/impl/LUCache.cpp 
    app/core/impl/AppBuilderImpl.cpp 
    app/reader/ReadThread.cpp 
    app/writer/impl/WriterThread.cpp 
    app/core/impl/FactoryImpl.cpp 
    app/reader/ReadManager.cpp 
    app/writer/WriteManager.cpp 
    app/io/impl/StringInput.cpp 
    app/core/Application.cpp 
    app/util/Util.cpp 
    app/input/InputProcessor.cpp		
    app/core/CacheElement.cpp 
    app/core/impl/CacheElementCollectionImpl.cpp
)

target_link_libraries (cache ${CMAKE_THREAD_LIBS_INIT})
