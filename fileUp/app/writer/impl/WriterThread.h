#ifndef WRITERTHREAD_H
#define WRITERTHREAD_H
#include <thread>
#include <app/core/AppBuilder.h>
using namespace std;
namespace  app
{
    namespace writer
    {
        class WriterThread
        {
        public:
            WriterThread(app::core::AppBuilder& appBuilder);
            void run(int threadId, std::string inputFile);
            thread* start(int threadId, std::string inputFile);
        private:
            app::core::AppBuilder& m_appBuilder;
            std::thread* m_thisThread;
        };
    }
}
#endif // WRITERTHREAD_H
