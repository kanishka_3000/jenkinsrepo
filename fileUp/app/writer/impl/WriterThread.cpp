#include "WriterThread.h"
#include <iostream>
app::writer::WriterThread::WriterThread(core::AppBuilder &appBuilder):
        m_appBuilder(appBuilder), m_thisThread(nullptr)
{

}

void app::writer::WriterThread::run(int threadId, string inputFile)
{
    std::thread::id this_id = std::this_thread::get_id();
    std::cout << "Thread Created with ID: " << this_id << "App Ref : " << threadId << endl;
    app::writer::Writer& writer = m_appBuilder.createWriter();
    writer.init(inputFile);
}

thread *app::writer::WriterThread::start(int threadId, string inputFile)
{
    m_thisThread =  new thread(&app::writer::WriterThread::run, this, threadId, inputFile);
    return m_thisThread;
}
