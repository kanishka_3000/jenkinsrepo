#ifndef WRITERIMPL_H
#define WRITERIMPL_H
#include <app/writer/Writer.h>
#include <app/core/DataManger.h>
#include <app/io/IoReader.h>
namespace  app
{
    namespace writer
    {
        class WriterImpl : public app::writer::Writer
        {
        public:
            WriterImpl(app::core::DataManger& dataManager, app::io::IoReader& reader);
            void init(string inputFileName) override;
        private:
            app::core::DataManger& m_dataManager;
            app::io::IoReader& m_reader;
        };
    }
}
#endif // WRITERIMPL_H
