#include "WriterImpl.h"
#include <app/io/impl/OutputData.h>
app::writer::WriterImpl::WriterImpl(app::core::DataManger& dataManager,
                                    app::io::IoReader& reader):
    m_dataManager(dataManager), m_reader(reader)
{

}

void app::writer::WriterImpl::init(string inputFileName)
{
     m_reader.init(inputFileName);
     app::io::OutputData data;
     app::io::IOStatus status;

     while(m_reader.readNext(data) == app::io::IOStatus::ERR_NONE)
     {
        int index;
        double value;
        status = data.getData(index, value);
        if(status == app::io::IOStatus::ERR_ERROR)
        {
            continue;
        }
        m_dataManager.newValue(index, value);
     }
}
