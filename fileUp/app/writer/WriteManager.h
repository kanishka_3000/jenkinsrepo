#ifndef WRITEMANAGER_H
#define WRITEMANAGER_H
#include <app/core/Factory.h>
#include <app/core/AppBuilder.h>
#include <vector>
#include <thread>

namespace  app
{
    namespace writer
    {
        class WriteManager
        {
        public:
            WriteManager(app::core::Factory& factory, app::core::AppBuilder& appBuilder);
            void init(const std::string& writerFile, std::vector<std::thread*>& createdThreads);
        private:
            app::core::Factory& m_factory;
            app::core::AppBuilder& m_appBuilder;
        };
    }
}
#endif // WRITEMANAGER_H
