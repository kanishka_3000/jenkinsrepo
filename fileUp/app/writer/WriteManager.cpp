#include "WriteManager.h"
#include <app/io/IoReader.h>
#include <app/writer/impl/WriterThread.h>
#include <app/io/impl/StringInput.h>
app::writer::WriteManager::WriteManager(core::Factory &factory,
                                        core::AppBuilder &appBuilder):
     m_factory(factory), m_appBuilder(appBuilder)
{

}

void app::writer::WriteManager::init(const string &writerFile, std::vector<thread *> &createdThreads)
{
    app::io::IoReader& reader = m_factory.createIoReader();
    reader.init(writerFile);

    app::io::StringInput stringInput;
    int threadCount = 0;
    while(reader.readNext(stringInput) != app::io::IOStatus::ERR_ERROR)
    {
        std::string fileName = stringInput.getDataLine();
        app::writer::WriterThread* writerThread = new app::writer::WriterThread(m_appBuilder);
        std::thread* th = writerThread->start(threadCount++, fileName);
        createdThreads.push_back(th);
    }
    m_factory.destroy(reader);
}
