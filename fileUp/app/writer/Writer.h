#ifndef WRITER_H
#define WRITER_H
#include <string>
using namespace std;
namespace  app
{
    namespace writer
    {
        class Writer
        {
        public:
            virtual ~Writer(){}
            virtual void init(std::string inputFileName) = 0;
        };
    }
}
#endif // WRITER_H
