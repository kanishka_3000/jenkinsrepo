#ifndef IOINPUT_H
#define IOINPUT_H

#include <string>
using namespace std;
namespace app
{
    namespace io
    {
        class IoInput
        {
        public:
            virtual ~IoInput(){}
            virtual std::string getData() const = 0;
        };
    }
}
#endif // IOINPUT_H
