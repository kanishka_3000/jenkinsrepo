#ifndef INPUTDATA_H
#define INPUTDATA_H

#include <app/io/IoOutput.h>
namespace app
{
    namespace io
    {
        class InputData : public app::io::IoOutput
        {
        public:
            InputData();
            void onDataLine(string data) override;
            IOStatus getData(int& data);
        private:
            std::string m_data;
        };
    }
}

#endif // INPUTDATA_H
