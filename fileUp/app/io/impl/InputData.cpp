#include "InputData.h"

app::io::InputData::InputData()
{

}

void app::io::InputData::onDataLine(string data)
{
    m_data = data;
}

app::io::IOStatus app::io::InputData::getData(int &data)
{
    if(m_data.length() == 0 || m_data == "")
    {
        return IOStatus::ERR_ERROR;
    }

    data = atoi(m_data.c_str());
    return IOStatus::ERR_NONE;
}
