#ifndef READERDUMPINPUT_H
#define READERDUMPINPUT_H
#include <app/io/IoInput.h>
#include <app/Defs.h>
namespace app
{
    namespace io
    {

        class ReaderDumpInput: public app::io::IoInput
        {
        public:
            ReaderDumpInput(Read_Source source, double value );
            string getData() const override;
        private:
            Read_Source m_source;
            double m_value;
        };
    }
}
#endif // READERDUMPINPUT_H
