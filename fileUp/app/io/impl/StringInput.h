#ifndef STRINGINPUT_H
#define STRINGINPUT_H

#include <app/io/IoOutput.h>
namespace app
{
    namespace io
    {
        class StringInput: public app::io::IoOutput
        {
        public:
            StringInput();
            void onDataLine(string data) override;
            std::string getDataLine() const;

        private:
            std::string m_data;
        };
    }
}
#endif // STRINGINPUT_H
