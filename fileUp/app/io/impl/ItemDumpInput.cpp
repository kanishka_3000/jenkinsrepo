#include "ItemDumpInput.h"
#include <stdio.h>
app::io::ItemDumpInput::ItemDumpInput(double value):
    m_value(value)
{

}

string app::io::ItemDumpInput::getData() const
{
    char zBuff[100];
    snprintf(zBuff, 99, "%f", m_value);

    return zBuff;
}
