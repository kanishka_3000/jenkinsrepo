#ifndef FILEREADER_H
#define FILEREADER_H

#include <string>
#include <fstream>

#include <app/io/IoReader.h>
using namespace std;

namespace app
{
    namespace io
    {
        class FileReader : public app::io::IoReader
        {
        public:
            FileReader();
            virtual ~FileReader();
            IOStatus getValue(int position, double& output) override;
            IOStatus readNext(IoOutput &output) override;
            int init(std::string fileName) override;

        private:
            std::string m_fileName;
            std::fstream m_fileStream;
        };
    }
}
#endif // FILEREADER_H
