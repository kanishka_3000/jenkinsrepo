#include "ReaderDumpInput.h"
#include <stdio.h>
app::io::ReaderDumpInput::ReaderDumpInput(Read_Source source, double value):
    m_source(source), m_value(value)
{

}

string app::io::ReaderDumpInput::getData() const
{
    char zData[100];
    switch (m_source)
    {
    case Read_Source::DISK:
    {
        snprintf(zData, 99, "%s %f", "Disk", m_value);
        break;
    }
    case Read_Source::CACHE:
    {
        snprintf(zData, 99, "%s %f", "Cache", m_value);
        break;
    }
    case Read_Source::NONE:
    {
        snprintf(zData, 99, "%f",  m_value);
        break;
    }
    default:
        break;
    }
    return zData;
}
