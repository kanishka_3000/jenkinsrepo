#include "FileWriter.h"
#include <cstdio>
#include <stdio.h>
#include <string.h>
app::io::FileWriter::FileWriter()
{

}

app::io::FileWriter::~FileWriter()
{

}

void app::io::FileWriter::init(string fileName)
{
    m_fileName = fileName;
    char tmpFileName[500];
    snprintf(tmpFileName, 499, "%s.tmp", fileName.c_str());
    m_tmpFileName = tmpFileName;
}

void app::io::FileWriter::append(const IoInput &input)
{
    fstream fileStream;
    fileStream.open(m_fileName.c_str(), std::fstream::out | std::fstream::app);
    std::string data = input.getData();
    char zBuff2[100];
    snprintf(zBuff2, 99, "%s\n", data.c_str());
    int len = strlen(zBuff2);
    fileStream.write(zBuff2, len);
    fileStream.flush();
    fileStream.close();
}

void app::io::FileWriter::insert(int index, const IoInput &input)
{
    if(index < 1)
    {
        return ;
    }
    std::fstream inputFileStream;
    std::fstream outputFileStream;
    inputFileStream.open(m_fileName.c_str(), std::fstream::in | std::fstream::out );
    outputFileStream.open(m_tmpFileName.c_str(),  std::fstream::out | std::fstream::trunc);

    int currentLine = 0;
    inputFileStream.seekg(0);
    char zBuff[100];
    while(inputFileStream.peek() != EOF)
    {
        inputFileStream.getline(zBuff, 99);
        currentLine++;
        if(currentLine == index)
        {
            snprintf(zBuff, 99, "%s", input.getData().c_str());

        }
        char zBuff2[100];
        snprintf(zBuff2, 99, "%s\n", zBuff);
        int dataLen = strlen(zBuff2);
        outputFileStream.write(zBuff2, dataLen);
        outputFileStream.flush();
    }
    if(currentLine < index)
    {
        for(int i = currentLine + 1 ; i < index; i++)
        {
            outputFileStream << "0" << endl;
            currentLine++;
        }

        outputFileStream << input.getData().c_str()  << endl;
    }
    inputFileStream.close();
    outputFileStream.close();
    rename(m_tmpFileName.c_str(), m_fileName.c_str());
}
