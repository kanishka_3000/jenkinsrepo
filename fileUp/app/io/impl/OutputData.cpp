#include "OutputData.h"

app::io::OutputData::OutputData()
{

}

void app::io::OutputData::onDataLine(string data)
{
    m_data = data;
}

app::io::IOStatus app::io::OutputData::getData(int &index, double &value)
{
    if(m_data.length() == 0 || m_data == "")
    {
        return IOStatus::ERR_ERROR;
    }
    std::size_t space = m_data.find(" ");
    string indexStr = m_data.substr(0, space);

    std::size_t beginVal = space + 1;
    std::size_t length = m_data.length() - beginVal;

    string valueStr = m_data.substr(beginVal, length);

    index = atoi(indexStr.c_str());
    value = atof(valueStr.c_str());

    return IOStatus::ERR_NONE;
}
