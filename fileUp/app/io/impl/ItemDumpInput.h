#ifndef ITEMDUMPINPUT_H
#define ITEMDUMPINPUT_H
#include <app/io/IoInput.h>

namespace app
{
    namespace io
    {
        class ItemDumpInput : public app::io::IoInput
        {
        public:
            ItemDumpInput(double value);
            string getData() const override;
         private:
            double m_value;
        };
    }
}
#endif // ITEMDUMPINPUT_H
