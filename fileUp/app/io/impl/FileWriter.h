#ifndef FILEWRITER_H
#define FILEWRITER_H
#include <fstream>
#include <string>
#include <app/io/IoWriter.h>

using namespace std;
namespace app
{
    namespace io
    {

        class FileWriter: public app::io::IoWriter
        {
        public:
            FileWriter();
            ~FileWriter();
            void insert(int index,  const app::io::IoInput& input) override;
            void init(std::string fileName) override;
            void append(const IoInput &input) override;
        private:
            std::string m_fileName;
            std::string m_tmpFileName;


        };
    }
}
#endif // FILEWRITER_H
