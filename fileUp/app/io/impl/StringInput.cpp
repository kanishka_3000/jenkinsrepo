#include "StringInput.h"

app::io::StringInput::StringInput()
{

}


void app::io::StringInput::onDataLine(string data)
{
    m_data = data;
}

string app::io::StringInput::getDataLine() const
{
    return m_data;
}
