#include "FileReader.h"

app::io::FileReader::FileReader()
{

}

app::io::FileReader::~FileReader()
{
    if(m_fileStream.is_open())
    {
        m_fileStream.close();
    }
}

app::io::IOStatus app::io::FileReader::getValue(int position, double &output)
{
    if(position < 1)
    {
        return IOStatus::ERR_ERROR;
    }

    int currentLine = 0;
    std::fstream fileStream;
    fileStream.open(m_fileName.c_str());
    fileStream.seekg(0);
    char zBuff[100];
    while(fileStream.peek() != EOF)
    {
        fileStream.getline(zBuff, 99);
        currentLine++;
        if(currentLine == position)
        {
            break;
        }
    }
    fileStream.close();
    if(currentLine == position)
    {
        output = atof(zBuff);
        return IOStatus::ERR_NONE;
    }
    return IOStatus::ERR_ERROR;
}

app::io::IOStatus app::io::FileReader::readNext(app::io::IoOutput &output)
{
    char zBuff[100];
    if(m_fileStream.peek() != EOF)
    {
         m_fileStream.getline(zBuff, 99);
         output.onDataLine(zBuff);
         return IOStatus::ERR_NONE;
    }
    return IOStatus::ERR_ERROR;
}

int app::io::FileReader::init(string fileName)
{
    m_fileName = fileName;
    m_fileStream.open(m_fileName.c_str());
    return 1;
}
