#ifndef OUTPUTDATA_H
#define OUTPUTDATA_H

#include <app/io/IoOutput.h>

namespace app
{
    namespace io
    {
        class OutputData: public app::io::IoOutput
        {
        public:
            OutputData();
            void onDataLine(string data) override;

            IOStatus getData(int& index, double& value);
        private:
            std::string m_data;

        };
    }
}
#endif // OUTPUTDATA_H
