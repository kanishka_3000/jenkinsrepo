#ifndef IOREADER_H
#define IOREADER_H

#include <string>
#include <app/io/IoOutput.h>
using namespace std;

namespace app
{
    namespace io
    {

        class IoReader
        {
        public:
            virtual ~IoReader(){}
            virtual int init(std::string fileName) = 0;

            virtual IOStatus getValue(int position, double& output) = 0;
            virtual IOStatus readNext(app::io::IoOutput& output) = 0;
        };
    }
}
#endif // IOREADER_H
