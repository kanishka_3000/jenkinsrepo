#ifndef IOOUTPUT_H
#define IOOUTPUT_H

#include <string>
using namespace std;

namespace app
{
    namespace io
    {
        enum class IOStatus
        {
            ERR_NONE,
            ERR_ERROR
        };
        class IoOutput
        {
        public:
            virtual ~IoOutput(){}
            virtual void onDataLine(std::string data) = 0;
        };
    }
}
#endif // IOOUTPUT_H
