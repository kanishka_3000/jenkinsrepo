#ifndef IOWRITER_H
#define IOWRITER_H
#include <string>

#include <app/io/IoInput.h>
namespace app
{
    namespace io
    {
        class IoWriter
        {
        public:
            virtual ~IoWriter(){}
            virtual void init(std::string fileName) = 0;
            virtual void insert(int index, const app::io::IoInput& input) = 0;
            virtual void append(const app::io::IoInput& input) = 0;
        };
    }
}
#endif // IOWRITER_H
