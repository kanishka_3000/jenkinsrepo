#include "Util.h"
#include <fstream>
#include <stdio.h>
app::util::Util::Util()
{

}

void app::util::Util::getFileContent(std::__cxx11::string fileName,
                                     std::list<std::__cxx11::string> &items) const
{
    std::fstream outputFile;
    outputFile.open(fileName,  std::fstream::in);
    char zBuff[100];
    while(outputFile.peek() != EOF)
    {
        outputFile.getline(zBuff, 99);

        items.push_back(zBuff);
    }
}

bool app::util::Util::fileExists(std::__cxx11::string fileName)
{
    FILE * pFile = fopen (fileName.c_str(),"r");
    if (pFile!= nullptr)
    {
        return true;
    }
    return false;
}

std::string app::util::Util::getReaderOutputName(std::string inputName)
{
    std::string name = inputName.append(".out");

    return name;
}
