#ifndef UTIL_H
#define UTIL_H
#include <string>
#include <list>
namespace app
{
    namespace util
    {

        class Util
        {
        public:
            Util();
            void getFileContent(std::string fileName, std::list<std::string>& items) const;
            bool fileExists(std::string fileName);
            static std::string getReaderOutputName(std::string inputName);
        };
    }
}
#endif // UTIL_H
