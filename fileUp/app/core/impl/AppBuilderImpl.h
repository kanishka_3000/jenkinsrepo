#ifndef APPBUILDERIMPL_H
#define APPBUILDERIMPL_H
#include <app/core/AppBuilder.h>
#include <app/core/DataManger.h>
namespace app
{
    namespace  core
    {
        class AppBuilderImpl: public app::core::AppBuilder
        {
        public:
            AppBuilderImpl(app::core::DataManger& dataManager);
            reader::Reader &createReader() override;
            writer::Writer &createWriter() override;
        private:
            app::core::DataManger& m_dataManager;
        };
    }
}
#endif // APPBUILDERIMPL_H
