#include "DataManagerImpl.h"
#include <app/io/impl/ItemDumpInput.h>
#include <thread>
#include <iostream>
app::core::DataManagerImpl::DataManagerImpl(Cache &cache, io::IoReader &reader, io::IoWriter &writer):
    m_cache(cache), m_reader(reader), m_writer(writer)
{

}

Data_Status app::core::DataManagerImpl::getValue(int index, double &value, Read_Source &source)
{
    double sourceValue = 0;
    std::unique_lock<std::mutex> lck (m_writeLock);
    //m_cache.debugDump();
    app::core::Cache::Cache_Result cacheResult = m_cache.getValue(index, sourceValue);

    if(cacheResult == app::core::Cache::Cache_Result::HIT)
    { 
        value = sourceValue;
        cout << "Cache Hit for: " << index << " : val : " << value << endl;
        source = Read_Source::CACHE;
        return Data_Status::ERR_NONE;
    }


    app::io::IOStatus status = m_reader.getValue(index, sourceValue);

    if(status == app::io::IOStatus::ERR_NONE)
    {
        value = sourceValue;
        cout << "Cache Miss for: " << index << " : val : " << value << endl;
        m_cache.onNewValue(index, sourceValue);
        source = Read_Source::DISK;
        return Data_Status::ERR_NONE;
    }
    cout << "Can not retrive from file: " << index << " : val : " << value << endl;
    return Data_Status::ERROR;
}

void app::core::DataManagerImpl::newValue(int index, double &value)
{

    std::unique_lock<std::mutex> lck (m_writeLock);
    //std::thread::id this_id = std::this_thread::get_id();
     cout << "Writing data for : " << index << " : val : " << value << endl;
    double sourceValue = 0;
    app::core::Cache::Cache_Result cacheResult = m_cache.getValue(index, sourceValue);
    if(cacheResult == app::core::Cache::Cache_Result::HIT)//should check if values too are different
    {
        m_cache.onNewValue(index, value);
    }
    m_writer.insert(index, app::io::ItemDumpInput(value));



}
