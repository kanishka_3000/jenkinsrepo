#ifndef DATAMANAGERIMPL_H
#define DATAMANAGERIMPL_H
#include <app/core/DataManger.h>
#include <app/core/Cache.h>
#include <app/io/IoReader.h>
#include <app/io/IoWriter.h>

#include <mutex>
namespace app
{
    namespace  core
    {
        class DataManagerImpl: public app::core::DataManger
        {
        public:
            DataManagerImpl(app::core::Cache& cache, app::io::IoReader& reader, app::io::IoWriter& writer);
            Data_Status getValue(int index, double& value, Read_Source& source) override;
            void newValue(int index, double &value) override;
        private:
            app::core::Cache& m_cache;
            app::io::IoReader& m_reader;
            app::io::IoWriter& m_writer;

            std::mutex m_writeLock;
        };
    }
}
#endif // DATAMANAGERIMPL_H
