#include "LUCache.h"

app::core::LUCache::LUCache(CacheElementCollection &collection):
    m_collection(collection)
{

}

void app::core::LUCache::init(unsigned int size)
{
    (void)size;
}

app::core::Cache::Cache_Result app::core::LUCache::getValue(int index, double &value)
{
    app::core::CacheElement element = m_collection.get(index);
    if(element.isValid() == false)
    {
        return Cache_Result::MISS;
    }
    value = element.getValue();
    return Cache_Result::HIT;
}

void app::core::LUCache::onNewValue(int index, double value)
{
    m_collection.put(index, app::core::CacheElement(value));
}


void app::core::LUCache::debugDump()
{
    m_collection.debugDump();
}
