#ifndef FACTORYIMPL_H
#define FACTORYIMPL_H

#include <app/core/Factory.h>
#include <app/io/impl/FileReader.h>
#include <app/io/impl/FileWriter.h>
#include <app/core/impl/LUCache.h>
#include <app/core/impl/DataManagerImpl.h>
#include <app/core/impl/AppBuilderImpl.h>
#include <app/core/impl/CacheElementCollectionImpl.h>

namespace app
{
    namespace  core
    {

        class FactoryImpl: public app::core::Factory
        {
        public:
            FactoryImpl();
            io::IoReader& createIoReader() const override;
            io::IoWriter& createIoWriter() const override;
            Cache& createCache(unsigned int size, app::core::CacheElementCollection& collection) const override;
            DataManger& createDataManager(app::core::Cache& cache, app::io::IoReader& reader, app::io::IoWriter&) const override;
            AppBuilder& createAppBuilder(DataManger &manager) const override;
            CacheElementCollection &createCacheElementCollection() const override;

            void destroy(io::IoReader &reader) const override;
            void destroy(io::IoWriter &writer) const override;




        };
    }
}
#endif // FACTORYIMPL_H
