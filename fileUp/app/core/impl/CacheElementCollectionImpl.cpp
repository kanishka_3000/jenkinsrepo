#include "CacheElementCollectionImpl.h"
#include <iostream>
app::core::CacheElementCollectionImpl::CacheElementCollectionImpl():
    m_maxSize(0)
{

}


void app::core::CacheElementCollectionImpl::init(unsigned int size)
{
    m_maxSize = size;
}

void app::core::CacheElementCollectionImpl::put(unsigned int index, app::core::CacheElement element)
{
    std::map<unsigned int, CacheElement>::const_iterator itr = m_data.find(index);
    if(itr != m_data.end())
    {
        //item not found in cache
        const CacheElement& existingElement = itr->second;
        element.setFrequency(existingElement.getFrequency());
        m_data[index] = element;
        return;
    }
    if(m_data.size() < m_maxSize)
    {
        m_data[index] = element;
        return;
    }
    unsigned int leasetUsedIndex = findLeasetUsedIndex();
    itr = m_data.find(leasetUsedIndex);
    m_data.erase(itr);

    m_data[index] = element;
}

app::core::CacheElement app::core::CacheElementCollectionImpl::get(unsigned int index)
{

    std::map<unsigned int, CacheElement>::iterator itr = m_data.find(index);
    if(itr == m_data.end())
    {
        //item not found in cache
        return app::core::CacheElement();
    }
    app::core::CacheElement& element = itr->second;
    element.increaseFrequency();
    return element;
}

unsigned int app::core::CacheElementCollectionImpl::findLeasetUsedIndex()
{
    int frequency = 1;
    unsigned int inx = 0;
    for(map<unsigned int, app::core::CacheElement>::iterator itr = m_data.begin();
        itr != m_data.end(); itr++)
    {
        unsigned int index = itr->first;
        app::core::CacheElement& ele = itr->second;
        int frq = ele.getFrequency();
        if(frq <= frequency)
        {
            frequency = frq;
            inx = index;
        }
    }
    return inx;
}


void app::core::CacheElementCollectionImpl::debugDump()
{
    std::cout << "======Debug Dump Cache Begin =======" << endl;
    for(map<unsigned int, app::core::CacheElement>::iterator itr = m_data.begin();
        itr != m_data.end(); itr++)
    {
        unsigned int index = itr->first;
        app::core::CacheElement& ele = itr->second;
        std::cout << "Index : " << index << " value : " << ele.getValue() << "Frq : " << ele.getFrequency() << endl;
    }
    std::cout << "======Debug Dump Cache End =======" << endl;
}
