#ifndef LUCACHE_H
#define LUCACHE_H
#include <app/core/Cache.h>
#include <app/core/CacheElementCollection.h>
namespace app
{
    namespace  core
    {
        class LUCache : public app::core::Cache
        {
        public:
            LUCache(app::core::CacheElementCollection& collection);
            void init(unsigned int size) override;
            Cache_Result getValue(int index, double &value) override;
            void onNewValue(int index, double value) override;
            void debugDump() override;
        private:
            app::core::CacheElementCollection& m_collection;



        };
    }
}
#endif // LUCACHE_H
