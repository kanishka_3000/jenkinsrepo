#ifndef CACHEELEMENTCOLLECTIONIMPL_H
#define CACHEELEMENTCOLLECTIONIMPL_H
#include <map>
#include <app/core/CacheElementCollection.h>
using namespace std;
namespace app
{
    namespace  core
    {
        class CacheElementCollectionImpl: public app::core::CacheElementCollection
        {
        public:
            CacheElementCollectionImpl();

            void init(unsigned int size) override;
            void put(unsigned int index, CacheElement element) override;
            CacheElement get(unsigned int index) override;
            void debugDump() override;
        private:
            unsigned int findLeasetUsedIndex();
            unsigned int m_maxSize;
            std::map<unsigned int, CacheElement> m_data;



        };
    }
}
#endif // CACHEELEMENTCOLLECTIONIMPL_H
