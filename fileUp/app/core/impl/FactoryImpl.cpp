#include "FactoryImpl.h"

app::core::FactoryImpl::FactoryImpl()
{

}


app::io::IoReader &app::core::FactoryImpl::createIoReader()const
{
    return *(new app::io::FileReader);
}

app::io::IoWriter &app::core::FactoryImpl::createIoWriter() const
{
    return *(new app::io::FileWriter);
}


void app::core::FactoryImpl::destroy(app::io::IoReader &reader) const
{
    auto rea = &reader;
    delete rea;
}

void app::core::FactoryImpl::destroy(app::io::IoWriter &writer) const
{
    auto wri = &writer;
    delete wri;
}


app::core::Cache &app::core::FactoryImpl::createCache(unsigned int size, app::core::CacheElementCollection& collection) const
{
    (void)size;
    auto var = new app::core::LUCache(collection);
    return *var;
}

app::core::DataManger &app::core::FactoryImpl::createDataManager(Cache &cache, io::IoReader &reader, io::IoWriter & writer) const
{
    app::core::DataManagerImpl*  manager = new app::core::DataManagerImpl(cache, reader, writer);
    return *manager;
}

app::core::AppBuilder &app::core::FactoryImpl::createAppBuilder(DataManger & manager) const
{
    app::core::AppBuilderImpl*  appBuilder = new app::core::AppBuilderImpl(manager);
    return *appBuilder;
}


app::core::CacheElementCollection &app::core::FactoryImpl::createCacheElementCollection() const
{
    auto val = new app::core::CacheElementCollectionImpl;
    return *val;
}
