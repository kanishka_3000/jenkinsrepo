#include "AppBuilderImpl.h"
#include <app/io/impl/FileReader.h>
#include <app/io/impl/FileWriter.h>
#include <app/reader/impl/ReaderImpl.h>
#include <app/writer/impl/WriterImpl.h>
app::core::AppBuilderImpl::AppBuilderImpl(app::core::DataManger &dataManager):
    m_dataManager(dataManager)
{

}


app::reader::Reader &app::core::AppBuilderImpl::createReader()
{
    //Objects will have the life of the application
    app::io::FileReader* fileReader = new app::io::FileReader;
    app::io::FileWriter* fileWriter = new app::io::FileWriter;

    app::reader::ReaderImpl* reader = new app::reader::ReaderImpl(
                m_dataManager, *fileReader, *fileWriter);

    return *reader;
}

app::writer::Writer &app::core::AppBuilderImpl::createWriter()
{
    //Objects will have the life of the application
    app::io::FileReader* fileReader = new app::io::FileReader;
    app::writer::WriterImpl* writer = new app::writer::WriterImpl(
                m_dataManager, *fileReader)    ;

    return *writer;
}
