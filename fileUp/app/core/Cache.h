#ifndef CACHE_H
#define CACHE_H

namespace app
{
    namespace  core
    {
        class Cache
        {
        public:
            enum class Cache_Result
            {
                HIT,
                MISS
            };

            virtual ~Cache(){}

            virtual void init(unsigned int size) = 0;
            virtual Cache_Result getValue(int index, double& value) = 0;
            virtual void onNewValue(int index, double value) = 0;
            virtual void debugDump() = 0;
        };
    }
}
#endif // CACHE_H
