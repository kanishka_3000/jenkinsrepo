#ifndef APPBUILDER_H
#define APPBUILDER_H
#include <app/reader/Reader.h>
#include <app/writer/Writer.h>
namespace app
{
    namespace  core
    {
        class AppBuilder
        {
        public:
            virtual ~AppBuilder(){}
            virtual app::reader::Reader& createReader() = 0;
            virtual app::writer::Writer& createWriter() = 0;
        };
    }
}
#endif // APPBUILDER_H
