#ifndef DATAMANGER_H
#define DATAMANGER_H
#include <app/Defs.h>
namespace app
{
    namespace  core
    {

        class DataManger
        {
        public:
            virtual ~DataManger(){}
            virtual Data_Status getValue(int index, double& value, Read_Source& source) = 0;
            virtual void newValue(int index, double& value) = 0;
        };

    }
}
#endif // DATAMANGER_H
