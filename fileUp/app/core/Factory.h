#ifndef FACTORY_H
#define FACTORY_H
#include <app/io/IoReader.h>
#include <app/io/IoWriter.h>
#include <app/core/Cache.h>
#include <app/core/DataManger.h>
#include <app/core/AppBuilder.h>
#include <app/core/CacheElementCollection.h>
namespace app
{
    namespace  core
    {
        class Factory
        {
        public:
            virtual ~Factory(){}
            virtual app::io::IoReader& createIoReader() const= 0;
            virtual app::io::IoWriter& createIoWriter() const = 0;
            virtual app::core::Cache& createCache(unsigned int size, app::core::CacheElementCollection& collection) const= 0;
            virtual app::core::DataManger& createDataManager(app::core::Cache& cache, app::io::IoReader& reader, app::io::IoWriter&) const = 0;
            virtual app::core::AppBuilder& createAppBuilder(DataManger &manager) const = 0;
            virtual app::core::CacheElementCollection& createCacheElementCollection() const = 0;

            virtual void destroy(app::io::IoReader& reader) const= 0;
            virtual void destroy(app::io::IoWriter& writer) const= 0;

        };
    }
}
#endif // FACTORY_H
