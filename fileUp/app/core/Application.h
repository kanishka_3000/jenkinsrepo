#ifndef APPLICATION_H
#define APPLICATION_H

#include <app/core/Factory.h>
#include <app/reader/ReadManager.h>
#include <app/writer/WriteManager.h>
namespace app
{
    namespace  core
    {
        enum class MODE
        {
            CONCURRENT,
            READERS_AFTER_WRITERS
        };

        class Application
        {
        public:
            Application(app::core::Factory& factory);
            void init(int cacheCount, std::string readersFile,
                      std::string writersFile, std::string dataFile, app::core::MODE mode);

        private:
            void readersAfterWriters(string writersFile, string readersFile, app::writer::WriteManager* writeManager, app::reader::ReadManager* readManager);
            void concurrent(string writersFile, string readersFile, app::writer::WriteManager* writeManager, app::reader::ReadManager* readManager);

            app::core::Factory& m_factory;
        };
    }
}
#endif // APPLICATION_H
