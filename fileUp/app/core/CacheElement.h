#ifndef CACHEELEMENT_H
#define CACHEELEMENT_H

namespace app
{
    namespace  core
    {
        class CacheElement
        {
        public:
            CacheElement();
            CacheElement(double value);
            bool isValid();

            int getFrequency() const;
            double getValue() const;
            void setValue(double value);
            void setFrequency(int frequency);
            void increaseFrequency();
        private:
            bool m_valid;
            int m_frequency;
            double m_value;
        };
    }
}
#endif // CACHEELEMENT_H
