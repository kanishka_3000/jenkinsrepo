#ifndef CACHEELEMENTCOLLECTION_H
#define CACHEELEMENTCOLLECTION_H
#include <app/core/CacheElement.h>

namespace app
{
    namespace  core
    {
        class CacheElementCollection
        {
        public:
            virtual ~CacheElementCollection(){}
            virtual void init(unsigned int size) = 0;
            virtual void put(unsigned int index, app::core::CacheElement element) = 0;
            virtual app::core::CacheElement get(unsigned int index) = 0;
            virtual void debugDump() = 0;
        };
    }
}
#endif // CACHEELEMENTCOLLECTION_H
