#include "CacheElement.h"

app::core::CacheElement::CacheElement():
    m_valid(false), m_frequency(0), m_value(0.0)
{

}
app::core::CacheElement::CacheElement(double value):
     m_valid(true), m_frequency(0)
{
    setValue(value);
}

bool app::core::CacheElement::isValid()
{
    return m_valid;
}

int app::core::CacheElement::getFrequency() const
{
    return m_frequency;
}

void app::core::CacheElement::setFrequency(int frequency)
{
    m_frequency = frequency;
}

void app::core::CacheElement::increaseFrequency()
{
    m_frequency = m_frequency + 1;
}

double app::core::CacheElement::getValue() const
{
    return m_value;
}

void app::core::CacheElement::setValue(double value)
{
    m_valid = true;
    m_value = value;
}
