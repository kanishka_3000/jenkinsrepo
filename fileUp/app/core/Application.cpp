#include "Application.h"


app::core::Application::Application(Factory &factory):
    m_factory(factory)
{

}

void app::core::Application::readersAfterWriters(string writersFile, string readersFile, app::writer::WriteManager* writeManager, app::reader::ReadManager* readManager)
{
    std::vector<std::thread*> createdThreads;
    writeManager->init(writersFile, createdThreads);
    for(std::thread* th : createdThreads)
    {
        th->join();
    }
    createdThreads.clear();
    readManager->init(readersFile, createdThreads);
    for(std::thread* th : createdThreads)
    {
        th->join();
    }
}

void app::core::Application::init(int cacheCount,
                                  string readersFile, string writersFile, string dataFile, MODE mode)
{
    app::io::IoReader& datReader = m_factory.createIoReader();
    datReader.init(dataFile);
    app::io::IoWriter& datWriter = m_factory.createIoWriter();
    datWriter.init(dataFile);

    app::core::CacheElementCollection& collection = m_factory.createCacheElementCollection();
    collection.init(cacheCount);
    app::core::Cache& cache = m_factory.createCache(cacheCount, collection);
    cache.init(cacheCount);

    app::core::DataManger& dataManager = m_factory.createDataManager(cache, datReader, datWriter);
    app::core::AppBuilder& appBuilder = m_factory.createAppBuilder(dataManager);



    app::reader::ReadManager* readManager = new  app::reader::ReadManager(m_factory, appBuilder);
    app::writer::WriteManager* writeManager = new app::writer::WriteManager(m_factory, appBuilder);

    switch (mode)
    {
        case app::core::MODE::READERS_AFTER_WRITERS:
        {
            readersAfterWriters(writersFile, readersFile, writeManager, readManager);
            break;
        }
        default:
        {
            concurrent(writersFile, readersFile, writeManager, readManager);
            break;
        }
    }



}

void app::core::Application::concurrent(string writersFile, string readersFile, app::writer::WriteManager *writeManager, app::reader::ReadManager *readManager)
{
    std::vector<std::thread*> createdThreads;
    writeManager->init(writersFile, createdThreads);
    readManager->init(readersFile, createdThreads);
    for(std::thread* th : createdThreads)
    {
        th->join();
    }
}
