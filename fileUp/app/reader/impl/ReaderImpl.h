#ifndef READERIMPL_H
#define READERIMPL_H
#include <app/reader/Reader.h>
#include <app/core/DataManger.h>
#include <app/io/IoReader.h>
#include <app/io/IoWriter.h>
namespace app
{
    namespace reader
    {
        class ReaderImpl: public app::reader::Reader
        {
        public:
            ReaderImpl(app::core::DataManger& dataManager, app::io::IoReader& reader, app::io::IoWriter& writer);
            void init(string inputFileName, string outputFileName) override;
        private:
            app::core::DataManger& m_dataManager;
            app::io::IoReader& m_reader;
            app::io::IoWriter& m_writer;
        };
    }
}
#endif // READERIMPL_H
