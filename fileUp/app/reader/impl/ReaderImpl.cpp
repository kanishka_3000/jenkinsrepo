#include "ReaderImpl.h"
#include <app/io/impl/InputData.h>
#include <app/Defs.h>
#include <app/io/impl/ReaderDumpInput.h>
app::reader::ReaderImpl::ReaderImpl(app::core::DataManger& dataManager, app::io::IoReader& reader, io::IoWriter &writer):
    m_dataManager(dataManager), m_reader(reader), m_writer(writer)
{

}

void app::reader::ReaderImpl::init(string inputFileName, string outputFileName)
{
    m_reader.init(inputFileName);
    m_writer.init(outputFileName);
    app::io::IOStatus status;
    app::io::InputData data;
    while(m_reader.readNext(data) == app::io::IOStatus::ERR_NONE)
    {
        int index = -1;
        status = data.getData(index);
        if(status == app::io::IOStatus::ERR_ERROR)
        {
            continue;
        }
        double data ;
        Read_Source source;
        Data_Status dataStatus = m_dataManager.getValue(index, data, source);
        if(dataStatus == Data_Status::ERROR)
        {
            //Not written to file yet, or unknown error
            data = 0;
            source = Read_Source::NONE;
        }
        m_writer.append(app::io::ReaderDumpInput(source, data));
    }
}
