#ifndef READTHREAD_H
#define READTHREAD_H
#include <app/core/AppBuilder.h>
#include <thread>
namespace app
{
    namespace reader
    {
        class ReadThread
        {
        public:
            ReadThread(app::core::AppBuilder& appBuilder);
            void run(int threadId, std::string inputFile, std::string outputFile);
            std::thread* start(int threadId, std::string inputFile, std::string outputFile);
        private:
           app::core::AppBuilder& m_appBuilder;
           std::thread* m_thisThread;
        };
    }
}
#endif // READTHREAD_H
