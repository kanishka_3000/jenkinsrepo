#include "ReadThread.h"
#include <iostream>
using namespace std;
app::reader::ReadThread::ReadThread(core::AppBuilder &appBuilder):
    m_appBuilder(appBuilder), m_thisThread(nullptr)
{

}

void app::reader::ReadThread::run(int threadId,
                                    std::string inputFile, std::string outputFile)
{
    std::thread::id this_id = std::this_thread::get_id();
    std::cout << "Thread Created with ID: " << this_id << "App Ref : " << threadId << endl;
    app::reader::Reader& reader = m_appBuilder.createReader();
    reader.init(inputFile, outputFile);

    //disassemble and delete through builder
}

std::thread* app::reader::ReadThread::start(int threadId,
                                            string inputFile, string outputFile)
{
    m_thisThread =  new thread(&app::reader::ReadThread::run,
                               this, threadId, inputFile, outputFile);
    return m_thisThread;
}
