#include "ReadManager.h"
#include <app/io/impl/StringInput.h>
#include <app/reader/ReadThread.h>
#include <sstream>
#include <app/util/Util.h>
app::reader::ReadManager::ReadManager(app::core::Factory& factory,
                                        core::AppBuilder &appBuilder):
    m_factory(factory), m_appBuilder(appBuilder)
{

}

void app::reader::ReadManager::init(const string &readerFile, std::vector<std::thread*>& createdThreads)
{
    app::io::IoReader& reader = m_factory.createIoReader();
    reader.init(readerFile);

    app::io::StringInput stringInput;
    int threadCount = 0;
    while(reader.readNext(stringInput) != app::io::IOStatus::ERR_ERROR)
    {
        std::string fileName = stringInput.getDataLine();
        app::reader::ReadThread* readerThread = new app::reader::ReadThread(m_appBuilder);
        std::stringstream ss;
        ss << "Reader_";
        ss << threadCount;
        std::string outputName = app::util::Util::getReaderOutputName(fileName);
        std::thread* th = readerThread->start(threadCount++, fileName, outputName);
        createdThreads.push_back(th);
    }
    m_factory.destroy(reader);
}
