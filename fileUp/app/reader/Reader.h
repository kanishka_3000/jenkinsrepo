#ifndef READER_H
#define READER_H
#include <string>

using namespace std;
namespace app
{
    namespace reader
    {
        class Reader
        {
        public:
            virtual ~Reader(){}
            virtual void init(std::string inputFileName, std::string outputFileName) = 0;
        };
    }
}
#endif // READER_H
