#ifndef READMANAGER_H
#define READMANAGER_H
#include <app/core/Factory.h>
#include <app/core/AppBuilder.h>
#include <vector>
#include <thread>
namespace app
{
    namespace reader
    {
        class ReadManager
        {
        public:
            ReadManager(app::core::Factory& factory, app::core::AppBuilder& appBuilder);
            void init(const std::string& readerFile, std::vector<std::thread*>& createdThreads);
        private:
            app::core::Factory& m_factory;
            app::core::AppBuilder& m_appBuilder;
        };
    }
}
#endif // READMANAGER_H
