#ifndef INPUTVALIDATOR_H
#define INPUTVALIDATOR_H
#include <string>
#include <list>
#include <app/util/Util.h>
using namespace std;
namespace app
{
    namespace input
    {
        class InputProcessor
        {
        public:
            InputProcessor(int argCount, char* argv[] );
            bool getInput(int& sizeOfCache, std::string& m_readerFile,
                          std::string& m_writerFile,
                          std::string& itemsFile, std::list<std::string>& errors );


            int getReaderThreadCount() const;
            int getWriterThreadCount() const;

            int getMode() const;

        private:
            bool checkFile(std::string fileName, int& count);

            bool m_valid;
            std::list<std::string> m_errors;
            app::util::Util m_util;

            std::string m_readerFile ;
            std::string m_writerFile ;
            std::string m_itemFile ;
            int m_sizeOfCache;
            int m_readerThreadCount;
            int m_writerThreadCount;

            int m_mode;
        };
    }
}
#endif // INPUTVALIDATOR_H
