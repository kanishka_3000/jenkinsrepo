#include "InputProcessor.h"
#include <sstream>
#include <iostream>
using namespace std;
app::input::InputProcessor::InputProcessor(int argCount, char* argv[]):
    m_valid(true), m_readerThreadCount(0), m_writerThreadCount(0), m_mode(1)
{
    if(argCount >= 7 || argCount < 5)
    {
        m_valid = false;
        m_errors.push_back("Invalid Argument Count should be 4 \n"
                           "<size_of_cache> <reader_file> <writer_file>"
                           " <items_file> <MODE 1 or 2>");
        return;
    }

    m_sizeOfCache = atoi(argv[1]);
    if(m_sizeOfCache < 1)
    {
        m_valid = false;
        m_errors.push_back("Cache Size can not be less than 1");
    }
    if(argCount == 6)
    {
        m_mode = atoi(argv[5]);

        if(m_mode > 2  || m_mode < 1)
        {
            m_valid = false;
            m_errors.push_back("mode 1 is concurrent mode 2 is readers after writers");
        }
    }
    m_readerFile = argv[2];
    m_writerFile = argv[3];
    m_itemFile = argv[4];
    bool fileEx = checkFile(m_readerFile, m_readerThreadCount);
    if(fileEx == false)
    {
        m_valid = false;
    }
    fileEx = checkFile(m_writerFile, m_writerThreadCount);
    if(fileEx == false)
    {
        m_valid = false;
    }
    fileEx = m_util.fileExists(m_itemFile);
    if(fileEx == false)
    {
        m_valid = false;
    }
}

bool app::input::InputProcessor::getInput(int &sizeOfCache,
                                          std::string &readerFile,
                                          std::string &writerFile, std::string &itemsFile,
                                          std::list<std::string> &errors)
{
    if(m_valid == false)
    {
        errors = m_errors;
        return false;
    }

    readerFile = m_readerFile;
    writerFile = m_writerFile;
    itemsFile = m_itemFile;
    sizeOfCache = m_sizeOfCache;
    return true;
}

bool app::input::InputProcessor::checkFile(string fileName, int &count)
{
    bool fileExists = m_util.fileExists(fileName);
    if(fileExists == false)
    {
        stringstream ss;
        ss << "File does not exist";
        ss << fileName.c_str();

        m_errors.push_back(ss.str());
        return false;
    }

    std::list<std::string> files;
    m_util.getFileContent(fileName, files);
    count = files.size();
    for(std::string file : files)
    {
        fileExists = m_util.fileExists(file);
        if(fileExists == false)
        {
            stringstream ss;
            ss << "File does not exist : ";
            ss << file.c_str();

            m_errors.push_back(ss.str());
            return false;
        }
    }
    return true;
}

int app::input::InputProcessor::getMode() const
{
    return m_mode;
}

int app::input::InputProcessor::getWriterThreadCount() const
{
    return m_writerThreadCount;
}

int app::input::InputProcessor::getReaderThreadCount() const
{
    return m_readerThreadCount;
}
