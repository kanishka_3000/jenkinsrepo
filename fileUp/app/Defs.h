#ifndef DEFS_H
#define DEFS_H
enum class Read_Source
{
    DISK,
    CACHE,
    NONE
};
enum class Data_Status
{
    ERR_NONE,
    ERROR
};

#endif // DEFS_H
