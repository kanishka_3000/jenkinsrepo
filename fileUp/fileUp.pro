TEMPLATE = app
CONFIG += console c++11 -pthread
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
	$$PWD/app/io/impl/FileReader.cpp \
	$$PWD/main.cpp \
    $$PWD/app/io/impl/InputData.cpp \
    $$PWD/app/io/impl/OutputData.cpp \
    $$PWD/app/io/impl/FileWriter.cpp \
    $$PWD/app/io/impl/ReaderDumpInput.cpp \
    $$PWD/app/core/impl/DataManagerImpl.cpp \
    $$PWD/app/reader/impl/ReaderImpl.cpp \
    $$PWD/app/writer/impl/WriterImpl.cpp \
    $$PWD/app/io/impl/ItemDumpInput.cpp \
    $$PWD/app/core/impl/LUCache.cpp \
    $$PWD/app/core/impl/AppBuilderImpl.cpp \
    $$PWD/app/reader/ReadThread.cpp \
    $$PWD/app/writer/impl/WriterThread.cpp \
    $$PWD/app/core/impl/FactoryImpl.cpp \
    $$PWD/app/reader/ReadManager.cpp \
    $$PWD/app/writer/WriteManager.cpp \
    $$PWD/app/io/impl/StringInput.cpp \
    $$PWD/app/core/Application.cpp \
    $$PWD/app/util/Util.cpp \
    $$PWD/app/input/InputProcessor.cpp \
    $$PWD/app/core/CacheElement.cpp \
    $$PWD/app/core/impl/CacheElementCollectionImpl.cpp

HEADERS += \
    $$PWD/app/io/impl/FileReader.h \
    $$PWD/app/io/IoReader.h \
    $$PWD/app/io/IoOutput.h \
    $$PWD/app/io/impl/InputData.h \
    $$PWD/app/io/impl/OutputData.h \
    $$PWD/app/io/IoWriter.h \
    $$PWD/app/io/impl/FileWriter.h \
    $$PWD/app/io/IoInput.h \
    $$PWD/app/io/impl/ReaderDumpInput.h \
    $$PWD/app/core/Cache.h \
    $$PWD/app/core/DataManger.h \
    $$PWD/app/Defs.h \
    $$PWD/app/core/impl/DataManagerImpl.h \
    $$PWD/app/reader/Reader.h \
    $$PWD/app/reader/impl/ReaderImpl.h \
    $$PWD/app/writer/Writer.h \
    $$PWD/app/writer/impl/WriterImpl.h \
    $$PWD/app/io/impl/ItemDumpInput.h \
    $$PWD/app/core/impl/LUCache.h \
    $$PWD/app/core/AppBuilder.h \
    $$PWD/app/core/impl/AppBuilderImpl.h \
    $$PWD/app/reader/ReadThread.h \
    $$PWD/app/writer/impl/WriterThread.h \
    $$PWD/app/core/Factory.h \
    $$PWD/app/core/impl/FactoryImpl.h \
    $$PWD/app/reader/ReadManager.h \
    $$PWD/app/writer/WriteManager.h \
    $$PWD/app/io/impl/StringInput.h \
    $$PWD/app/core/Application.h \
    $$PWD/app/input/InputValidator.h \
    $$PWD/app/util/Util.h \
    $$PWD/app/input/InputProcessor.h \
    $$PWD/app/core/CacheElement.h \
    $$PWD/app/core/CacheElementCollection.h \
    $$PWD/app/core/impl/CacheElementCollectionImpl.h
LIBS += -pthread
