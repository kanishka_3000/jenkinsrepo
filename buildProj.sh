#!/bin/bash
set -e
cd build
rm -rf *
cmake ../fileUp
make cache
echo "Coping the binary to bin.."
cp cache ../bin
echo "Coping resources.."
cp ../resources/* ../bin
