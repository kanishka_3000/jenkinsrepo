#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <app/input/InputProcessor.h>
#include <TestUtils.h>
#include <iostream>
using namespace std;
class InputProcessorTest: public ::testing::Test
{
public:
    void SetUp()
    {

    }
    void TearDown()
    {

    }
protected:
    TestUtils m_utils;
};

TEST_F(InputProcessorTest, whenCorrectArguments_ValidfilesReturned)
{
    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Reader2.txt", {"6", "1", "7"});
    m_utils.makeFile("Reader3.txt", {"7", "3", "4"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt", "Reader2.txt", "Reader3.txt"});

    m_utils.makeFile("Writer1.txt", {"1 56.66", "7 545.33", "2 566.66623"});
    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);

    std::list<string> emptylit;
    m_utils.makeFile("DataFileVal.dat", emptylit);

    char* zArgs[5];
    zArgs[1] = (char*)"11";
    zArgs[2] = (char*)"Readers.txt";
    zArgs[3] = (char*)"Writers.txt";
    zArgs[4] = (char*)"DataFileVal.dat";

    std::list<std::string> errorList;
    app::input::InputProcessor processor(5, zArgs);
    std::string inputFile;
    std::string outputFile;
    std::string dataFile;
    int sizeOfCache;
    bool processed = processor.getInput(sizeOfCache,
                                        inputFile, outputFile, dataFile, errorList);
    EXPECT_TRUE(processed);
    for(string s : errorList)
    {
        cout << s.c_str() << endl;
    }
    EXPECT_EQ(sizeOfCache, 11);
    EXPECT_STREQ("Readers.txt", inputFile.c_str());
    EXPECT_STREQ("Writers.txt", outputFile.c_str());
    EXPECT_STREQ("DataFileVal.dat", dataFile.c_str());

    m_utils.deleteFiles({
                            "Readers.txt",
                            "Reader1.txt",
                            "Reader2.txt",
                            "Reader3.txt",
                            "Writers.txt",
                            "Writer1.txt",
                            "Writer2.txt",
                            "Writer3.txt",
                            "DataFileVal.dat"
                        });

}

TEST_F(InputProcessorTest, whenInCorrectArguments_ErrorsReturned)
{
    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Reader3.txt", {"7", "3", "4"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt", "Reader2.txt", "Reader3.txt"});


    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);

    std::list<string> emptylit;
    m_utils.makeFile("DataFileVal.dat", emptylit);

    char* zArgs[5];
    zArgs[1] = (char*)"11";
    zArgs[2] = (char*)"Readers.txt";
    zArgs[3] = (char*)"Writers.txt";
    zArgs[4] = (char*)"DataFileVal.dat";

    std::list<std::string> errorList;
    app::input::InputProcessor processor(5, zArgs);
    std::string inputFile;
    std::string outputFile;
    std::string dataFile;
    int sizeOfCache;
    bool processed = processor.getInput(sizeOfCache,
                                        inputFile, outputFile, dataFile, errorList);
    EXPECT_FALSE(processed);
    for(string s : errorList)
    {
        cout << s.c_str() << endl;
    }


    m_utils.deleteFiles({
                            "Readers.txt",
                            "Reader1.txt",
                            "Reader3.txt",
                            "Writers.txt",
                            "Writer2.txt",
                            "Writer3.txt",
                            "DataFileVal.dat"
                        });

}
