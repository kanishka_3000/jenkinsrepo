#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <app/io/impl/FileWriter.h>
#include <app/io/impl/ReaderDumpInput.h>
#include <TestUtils.h>
#include <mocks/MockIoInput.h>
#include <app/Defs.h>
class FileWriter: public ::testing::Test
{
public:
    void SetUp()
    {
        m_fileWriter = new app::io::FileWriter();

    }
    void TearDown()
    {
        delete m_fileWriter;
        m_fileWriter = nullptr;
    }
protected:
    app::io::FileWriter* m_fileWriter;
    app::io::MockIoInput m_input;

    TestUtils m_utils;
};

TEST_F(FileWriter, whenFileWriteReq_thenCorrectLineIsUpdated)
{
     m_fileWriter->init("myfile.txt");
    list<double> sample  ={78 , 4654.35, 343.21, 335.35, 44, 33};
    m_utils.makeFile("myfile.txt", sample);

    EXPECT_CALL(m_input, getData()).
            WillOnce(testing::Return("9090.9095")).
            WillOnce(testing::Return("8080.80"));
    m_fileWriter->insert(2, m_input);

    list<double> output;
    m_utils.getFileData("myfile.txt", output);

    for(double d : output)
    {
        cout << "File Data is : " << d << endl;
    }
    EXPECT_THAT(output, testing::ElementsAre(78 , 9090.9095, 343.21, 335.35, 44, 33));

     m_fileWriter->insert(3, m_input);
     list<double> output2;
     m_utils.getFileData("myfile.txt", output2);
     cout << "------------" << endl;
     for(double d : output2)
     {
         cout << "File Data is : " << d << endl;
     }
     EXPECT_THAT(output2, testing::ElementsAre(78 , 9090.9095, 8080.8, 335.35, 44, 33));
}

TEST_F(FileWriter, whenIoInputAppend_thenCorrectValueAppendedTofile)
{
    list<double> sample  ={78 , 4654.35, 343.21, 335.35, 44, 33};
    m_utils.makeFile("myfile2.txt", sample);
    EXPECT_CALL(m_input, getData()).
            WillOnce(testing::Return("6776.9095")).
            WillOnce(testing::Return("1010.80"));

     m_fileWriter->init("myfile2.txt");
     m_fileWriter->append(m_input);

     list<double> output2;
     m_utils.getFileData("myfile2.txt", output2);
    EXPECT_THAT(output2, testing::ElementsAre(78 , 4654.35, 343.21, 335.35, 44, 33, 6776.9095));
    m_fileWriter->append(m_input);

    list<double> output3;
    m_utils.getFileData("myfile2.txt", output3);
    EXPECT_THAT(output3, testing::ElementsAre(78 , 4654.35, 343.21, 335.35, 44, 33, 6776.9095, 1010.80));
}

TEST_F(FileWriter, whenFileWriteToNewLine_thenCorrectLineIsUpdatedWithPathUpdate)
{
    list<double> sample  ={ 33};
    m_utils.makeFile("myfile3.txt", sample);
    EXPECT_CALL(m_input, getData()).
            WillOnce(testing::Return("6776.9095"));

     m_fileWriter->init("myfile3.txt");

     m_fileWriter->insert(5, m_input);
     list<double> output2;
     m_utils.getFileData("myfile3.txt", output2);

     EXPECT_THAT(output2, testing::ElementsAre( 33.0, 0.0, 0.0, 0.0, 6776.9095));
}

TEST_F(FileWriter, whenFileWriteToEmptyNewLine_thenCorrectLineIsUpdatedWithPathUpdate)
{
    list<double> sample  ={ };
    m_utils.makeFile("myfile3.txt", sample);
    EXPECT_CALL(m_input, getData()).
            WillOnce(testing::Return("6776.9095"));

     m_fileWriter->init("myfile3.txt");

     m_fileWriter->insert(5, m_input);
     list<double> output2;
     m_utils.getFileData("myfile3.txt", output2);

     EXPECT_THAT(output2, testing::ElementsAre( 0.0, 0.0, 0.0, 0.0, 6776.9095));
}

TEST_F(FileWriter, whenIoInputChecked_thenCorrectValueisReturned)
{
    app::io::ReaderDumpInput dump(Read_Source::CACHE, 34.53);

    std::string output = dump.getData();

    EXPECT_STREQ(output.c_str(), "Cache 34.530000");

    app::io::ReaderDumpInput dump2(Read_Source::DISK, 65.53);

    output = dump2.getData();

    EXPECT_STREQ(output.c_str(), "Disk 65.530000");
}
