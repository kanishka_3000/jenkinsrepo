#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <list>
#include <TestUtils.h>
#include <app/core/impl/DataManagerImpl.h>
#include <app/io/impl/FileReader.h>
#include <app/io/impl/FileWriter.h>
#include <app/core/impl/LUCache.h>
#include <app/reader/ReadManager.h>
#include <thread>
#include <app/core/impl/FactoryImpl.h>
#include <app/core/impl/AppBuilderImpl.h>
#include <app/writer/WriteManager.h>
#include <sstream>

class WriterThreadTest: public ::testing::Test
{
public:
    void SetUp()
    {

    }
    void TearDown()
    {

    }
protected:
    TestUtils m_utils;
};

TEST_F(WriterThreadTest, whenWritersInitiatedWithNonConflictingIndices_positionsAreWritten)
{
   
    m_utils.makeFile("Dat2File2.dat", {45.45, 66,66});
    app::io::FileReader reader;
    reader.init("DataFile2.dat");

    app::io::FileWriter writer;
    writer.init("Dat2File2.dat");
    app::core::CacheElementCollectionImpl collection;
    collection.init(5);
    app::core::LUCache luCache(collection);

    app::core::DataManagerImpl manager(luCache, reader, writer);
    app::core::AppBuilderImpl appBuilder(manager);
    app::core::FactoryImpl factory;

    app::writer::WriteManager wrMainager(factory, appBuilder);

    std::vector<std::thread*> createdThreads;
    m_utils.makeFile("Writer1.txt", {"1 56.66", "3 545.33", "2 566.66623"});
    m_utils.makeFile("Writers.txt", {"Writer1.txt"});

    wrMainager.init("Writers.txt", createdThreads);

    for(std::thread* th : createdThreads)
    {
        th->join();
    }
    std::list<double> output;
    m_utils.getFileData("Dat2File2.dat", output);
    EXPECT_THAT(output, testing::ElementsAre(56.66, 566.66623, 545.33));
    m_utils.deleteFile("Dat2File2.dat");

}

TEST_F(WriterThreadTest, whenWritersInitiatedWithNonConflictingIndicesMuliple_positionsAreWritten)
{
    std::list<double> input;
    m_utils.makeFile("Dat2File2.dat", input);
    app::io::FileReader reader;
    reader.init("Dat2File2.dat");

    app::io::FileWriter writer;
    writer.init("Dat2File2.dat");//Reader and Writer same file

    app::core::CacheElementCollectionImpl collection;
    collection.init(5);
    app::core::LUCache luCache(collection);

    app::core::DataManagerImpl manager(luCache, reader, writer);
    app::core::AppBuilderImpl appBuilder(manager);
    app::core::FactoryImpl factory;

    app::writer::WriteManager wrMainager(factory, appBuilder);

    std::vector<std::thread*> createdThreads;
    m_utils.makeFile("Writer1.txt", {"1 56.66", "7 545.33", "2 566.66623"});
    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);

    wrMainager.init("Writers.txt", createdThreads);

    for(std::thread* th : createdThreads)
    {
        th->join();
    }
    std::list<double> output;
    m_utils.getFileData("Dat2File2.dat", output);
    std::vector<double> exp = {56.66, 566.66623, -78.56, -78.56, 3423.663,
                               9090.90, 545.33, 0.0, 9090.90, 3423.663};
    EXPECT_THAT(output, testing::ElementsAreArray(exp));
    m_utils.deleteFile("Dat2File2.dat");
}

