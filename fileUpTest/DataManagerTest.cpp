#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <app/core/impl/DataManagerImpl.h>
#include <mocks/MockCache.h>
#include <mocks/MockIoReader.h>
#include <mocks/MockIoWriter.h>
class DataManagerTest: public ::testing::Test
{
public:
    void SetUp()
    {
        m_dataManger = new app::core::DataManagerImpl(m_cache, m_reader, m_writer);
    }
    void TearDown()
    {
        delete m_dataManger;
        m_dataManger = nullptr;
    }
protected:

    app::core::DataManger* m_dataManger;

    app::core::MockCache m_cache;
    app::io::MockIoReader m_reader;
    app::io::MockIoWriter m_writer;
};

TEST_F(DataManagerTest, whenDataRequestd_thenRetrivedFromCache)
{
    EXPECT_CALL(m_cache, getValue(testing::_, testing::_)).
            WillOnce(testing::Invoke(
                                [&](int index, double& value){
                                    value = 245.667;
                                    EXPECT_EQ(index, 5);
                                    return app::core::Cache::Cache_Result::HIT;
                                })).
            WillOnce(testing::Invoke(
                         [&](int index, double& value){
                             EXPECT_EQ(index, 6);
                             (void)value;
                             return app::core::Cache::Cache_Result::MISS;
                         }));
     double value;
     Read_Source source;
     Data_Status status = m_dataManger->getValue(5, value, source);
     ASSERT_EQ(status , Data_Status::ERR_NONE);

    EXPECT_EQ(value, 245.667);
    EXPECT_EQ(source, Read_Source::CACHE);

    EXPECT_CALL(m_reader, getValue(6, testing::_)).
            WillOnce(testing::Invoke(
                         [&](int position, double& output)
                {
                    (void)position;
                    output = 8945.355;
                    return app::io::IOStatus::ERR_NONE;
                }
                         ));
    status = m_dataManger->getValue(6, value, source);
    ASSERT_EQ(status , Data_Status::ERR_NONE);

   EXPECT_EQ(value, 8945.355);
   EXPECT_EQ(source, Read_Source::DISK);

}


