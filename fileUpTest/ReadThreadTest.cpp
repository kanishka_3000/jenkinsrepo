#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <app/reader/ReadThread.h>
#include <mocks/MockAppBuilder.h>
#include <list>
#include <TestUtils.h>
#include <app/core/impl/DataManagerImpl.h>
#include <app/io/impl/FileReader.h>
#include <app/io/impl/FileWriter.h>
#include <app/core/impl/LUCache.h>
#include <app/reader/ReadManager.h>
#include <thread>
#include <app/core/impl/FactoryImpl.h>
#include <app/core/impl/AppBuilderImpl.h>
#include <sstream>
class ReadThreadTest: public ::testing::Test
{
public:
    void SetUp()
    {
        m_readerThread = new app::reader::ReadThread(m_appBuilder);
    }
    void TearDown()
    {
        delete m_readerThread;
        m_readerThread = nullptr;
    }
protected:
    app::core::MockAppBuilder m_appBuilder;
    app::reader::ReadThread* m_readerThread;

    TestUtils m_utils;
};

TEST_F(ReadThreadTest, DISABLED_whenNewThreadStarts_thenExpectOutput)
{
    std::vector<std::thread*> threads;
    for(int i = 0 ; i < 10 ; i++)
    {
        app::reader::ReadThread* readerThread2 = new app::reader::ReadThread(m_appBuilder);
        std::thread* thr = readerThread2->start(i, "a", "");
        threads.push_back(thr);
    }

    for(auto td1 : threads)
    {
        td1->join();
    }

    std::thread* td = m_readerThread->start(250, "", "");

    td->join();


}

TEST_F(ReadThreadTest, whenFileReadINTEGRATED_thenReadOutputIsGeneraged)
{

    m_utils.makeFile("DataFile.dat", {45.45, 90.45, 356.3434, 9394.345});
    app::io::FileReader reader;
    reader.init("DataFile.dat");

    app::io::FileWriter writer;
    writer.init("Dat2File.dat");
    app::core::CacheElementCollectionImpl collection;
    collection.init(1);
    app::core::LUCache luCache(collection);

    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt"});
    m_utils.deleteFile("Reader1.txt.out");
    app::core::DataManagerImpl manager(luCache, reader, writer);
    app::core::AppBuilderImpl appBuilder(manager);
    app::core::FactoryImpl factory;
    app::reader::ReadManager readManager(factory, appBuilder);
    std::vector<std::thread*> createdThreads;
    readManager.init("Readers.txt", createdThreads);

    for(std::thread* th : createdThreads)
    {
        th->join();
    }
    std::list<std::string> reader1;
    m_utils.getFileData("Reader1.txt.out", reader1);
    EXPECT_THAT(reader1, testing::ElementsAre("Disk 45.450000", "Disk 356.343400", "Disk 90.450000"));
}

TEST_F(ReadThreadTest, whenFileReadThreadedINTEGRATED_thenReadOutputIsGeneraged)
{

    m_utils.makeFile("DataFile.dat", {45.45, 90.45, 356.3434, 9394.345, 0.0, -23.56, 345.99});
    app::io::FileReader reader;
    reader.init("DataFile.dat");

    app::io::FileWriter writer;
    writer.init("Dat2File.dat");
    app::core::CacheElementCollectionImpl collection;
    collection.init(1);
    app::core::LUCache luCache(collection);

    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Reader2.txt", {"6", "1", "7"});
    m_utils.makeFile("Reader3.txt", {"7", "3", "4"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt", "Reader2.txt", "Reader3.txt"});
    m_utils.deleteFile("Reader1.txt.out");
    m_utils.deleteFile("Reader2.txt.out");
    m_utils.deleteFile("Reader3.txt.out");

    app::core::DataManagerImpl manager(luCache, reader, writer);
    app::core::AppBuilderImpl appBuilder(manager);
    app::core::FactoryImpl factory;
    app::reader::ReadManager readManager(factory, appBuilder);
    std::vector<std::thread*> createdThreads;
    readManager.init("Readers.txt", createdThreads);

    for(std::thread* th : createdThreads)
    {
        th->join();
    }
    std::list<std::string> reader1;
    std::list<std::string> reader2;
    std::list<std::string> reader3;

    m_utils.getFileData("Reader1.txt.out", reader1);
    m_utils.getFileData("Reader2.txt.out", reader2);
    m_utils.getFileData("Reader3.txt.out", reader3);

    EXPECT_THAT(reader1, testing::ElementsAre(testing::AnyOf("Disk 45.450000", "Cache 45.450000"), testing::AnyOf("Disk 356.343400", "Cache 356.343400"), "Disk 90.450000"));
    EXPECT_THAT(reader2, testing::ElementsAre("Disk -23.560000", testing::AnyOf("Disk 45.450000", "Cache 45.450000"), testing::AnyOf("Disk 345.990000", "Cache 345.990000")));
    EXPECT_THAT(reader3, testing::ElementsAre(testing::AnyOf("Disk 345.990000", "Cache 345.990000"), testing::AnyOf("Disk 356.343400", "Cache 356.343400"), "Disk 9394.345000"));
}
