TEMPLATE = app
CONFIG += console c++11 -pthread
CONFIG -= app_bundle
CONFIG -= qt
include($$PWD/../fileUp/fileUp.pri)

INCLUDEPATH += "../fileUp"
DEPENDPATH +="../fileUp" ""

LIBS += -L/usr/local/lib -lgtest -lgmock

SOURCES += main.cpp \
    FileReaderTest.cpp \
    FileWriterTest.cpp \
    TestUtils.cpp \
    DataManagerTest.cpp \
    ReadThreadTest.cpp \
    WriterThreadTest.cpp \
    ApplicationTest.cpp \
    InputProcessorTest.cpp \
    CacheElementCollectionTest.cpp
LIBS += -pthread

HEADERS += \
    mocks/MockIoOutput.h \
    mocks/MockIoInput.h \
    TestUtils.h \
    mocks/MockCache.h \
    mocks/MockIoReader.h \
    mocks/MockIoWriter.h \
    mocks/MockAppBuilder.h
