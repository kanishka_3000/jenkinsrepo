#include <iostream>
#include <gtest/gtest.h>
#include <app/io/impl/FileReader.h>
using namespace std;

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    //::testing::GTEST_FLAG(filter) = "ApplicationTest.whenIntegratedTestSimple_thenCacheHit";
    return RUN_ALL_TESTS();

}
