#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <app/core/impl/CacheElementCollectionImpl.h>
class CacheElementCollectionTest: public ::testing::Test
{
public:
    void SetUp()
    {
        m_cacheCollection = new app::core::CacheElementCollectionImpl;
    }
    void TearDown()
    {
        delete m_cacheCollection;
        m_cacheCollection = nullptr;
    }
    void getMultiple(int index,unsigned int times)
    {
        for(unsigned int i = 0; i < times; i++)
        {
            m_cacheCollection->get(index);
        }
    }

protected:
    app::core::CacheElementCollection* m_cacheCollection;
};

TEST_F(CacheElementCollectionTest, whenItemsWithinLimitAdded_thenItemsCouldBeRetrived)
{
   m_cacheCollection->init(5);
   m_cacheCollection->put(8,app::core::CacheElement(56.34));
   m_cacheCollection->put(15,app::core::CacheElement(8949.45));
   m_cacheCollection->put(21,app::core::CacheElement(232.24));
   m_cacheCollection->put(0,app::core::CacheElement(545.223));

   app::core::CacheElement element = m_cacheCollection->get(15);
   ASSERT_TRUE(element.isValid());

   EXPECT_DOUBLE_EQ(element.getValue(), 8949.45);
}

TEST_F(CacheElementCollectionTest, whenItemExceedLimit_thenLeasedUsedItemIsRemoved)
{
    m_cacheCollection->init(3);
    m_cacheCollection->put(8,app::core::CacheElement(56.34));
    m_cacheCollection->put(15,app::core::CacheElement(8949.45));
    m_cacheCollection->put(21,app::core::CacheElement(232.24));
    getMultiple(8, 2);
    getMultiple(21, 2);

    app::core::CacheElement element = m_cacheCollection->get(15);
    ASSERT_TRUE(element.isValid());

    ASSERT_DOUBLE_EQ(element.getValue(), 8949.45);

    m_cacheCollection->put(90,app::core::CacheElement(8080.45));
    element = m_cacheCollection->get(90);
    ASSERT_TRUE(element.isValid());

    ASSERT_DOUBLE_EQ(element.getValue(), 8080.45);

    element = m_cacheCollection->get(15);
    ASSERT_FALSE(element.isValid());

}
