#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <list>

#include <app/io/impl/FileReader.h>
#include <app/io/impl/InputData.h>
#include <app/io/impl/OutputData.h>
#include <mocks/MockIoOutput.h>
#include <TestUtils.h>
class FileReaderTest: public ::testing::Test
{
public:
    void SetUp()
    {
        m_reader = new app::io::FileReader();
        m_utils.makeFile("dataFile.dat", {"1","4.78","6","64","34.345","46.46"});
        m_reader->init("dataFile.dat");
    }
    void TearDown()
    {
        delete m_reader;
        m_reader = nullptr;
        m_utils.deleteFile("dataFile.dat");
    }
protected:
    app::io::FileReader* m_reader;
    app::io::MockIoOutput m_mockOutput;
    TestUtils m_utils;
};

TEST_F(FileReaderTest, whenValueReadFromReader_thenExpectedDataReturned)
{
    double value = 0;
    m_reader->getValue(4, value);
    EXPECT_EQ(value, 64);
    std::cout << "Output is :" << value << endl;
    m_reader->getValue(5, value);
    std::cout << "Output is :" << value << endl;
    EXPECT_EQ(value, 34.345);//might need approximate eq

}

TEST_F(FileReaderTest, whenInvalidReadFromReader_then0Returned)
{
    double value = 0;
    app::io::IOStatus status = m_reader->getValue(114, value);
    ASSERT_EQ(status, app::io::IOStatus::ERR_ERROR);
    EXPECT_EQ(value, 0);
    std::cout << "Output is :" << value << endl;
    m_reader->getValue(5, value);
    std::cout << "Output is :" << value << endl;
    EXPECT_EQ(value, 34.345);//might need approximate eq
}

TEST_F(FileReaderTest, whenNextLineRead_thenCorrectOutIsReturned)
{
    std::string output;
    app::io::IOStatus status;
    EXPECT_CALL(m_mockOutput, onDataLine(testing::_)).
            WillRepeatedly(testing::Invoke(
                               [&](std::string dataLine)
    {
        output = dataLine;
    }
                               ));
    status = m_reader->readNext(m_mockOutput);
    ASSERT_EQ(status,  app::io::IOStatus::ERR_NONE);
    EXPECT_STREQ(output.c_str(), "1");
    cout << "Read : " << output.c_str() << endl;

    status = m_reader->readNext(m_mockOutput);
    EXPECT_STREQ(output.c_str(), "4.78");
    cout << "Read : " << output.c_str() << endl;

    status = m_reader->readNext(m_mockOutput);
    EXPECT_STREQ(output.c_str(), "6");
    cout << "Read : " << output.c_str() << endl;

    status = m_reader->readNext(m_mockOutput);
    status = m_reader->readNext(m_mockOutput);

    status = m_reader->readNext(m_mockOutput);
    EXPECT_STREQ(output.c_str(), "46.46");
    cout << "Read : " << output.c_str() << endl;

    status = m_reader->readNext(m_mockOutput);

    ASSERT_EQ(status,  app::io::IOStatus::ERR_ERROR);

}

TEST_F(FileReaderTest, whenInputDataProvided_thenCorrectOutIsReturned)
{
    app::io::InputData inputData;
    inputData.onDataLine("45\n");

    int output = -1;
    app::io::IOStatus status = inputData.getData(output);
    ASSERT_EQ(status , app::io::IOStatus::ERR_NONE);

    EXPECT_EQ(output, 45);
}

TEST_F(FileReaderTest, whenOutputDataProvided_thenCorrectOutIsReturned)
{
    app::io::OutputData outputData;
    outputData.onDataLine("39 3443.56\n");

    int index;
    double value;
    app::io::IOStatus status = outputData.getData(index, value);
    ASSERT_EQ(status , app::io::IOStatus::ERR_NONE);

    EXPECT_EQ(index, 39);
    EXPECT_EQ(value, 3443.56);
}
