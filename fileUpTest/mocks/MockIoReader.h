namespace app {
namespace io {

class MockIoReader : public IoReader {
 public:
  MOCK_METHOD1(init,
      int(std::string fileName));
  MOCK_METHOD2(getValue,
      IOStatus(int position, double& output));
  MOCK_METHOD1(readNext,
      IOStatus(app::io::IoOutput& output));
};

}  // namespace io
}  // namespace app
