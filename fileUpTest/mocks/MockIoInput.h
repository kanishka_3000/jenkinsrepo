namespace app {
namespace io {

class MockIoInput : public IoInput {
 public:
  MOCK_CONST_METHOD0(getData,
      std::string());
};

}  // namespace io
}  // namespace app
