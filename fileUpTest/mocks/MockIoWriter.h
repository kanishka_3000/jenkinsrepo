namespace app {
namespace io {

class MockIoWriter : public IoWriter {
 public:
  MOCK_METHOD1(init,
      void(std::string fileName));
  MOCK_METHOD2(insert,
      void(int index, const app::io::IoInput& input));
  MOCK_METHOD1(append,
      void(const app::io::IoInput& input));
};

}  // namespace io
}  // namespace app
