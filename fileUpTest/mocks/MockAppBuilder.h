namespace app {
namespace core {

class MockAppBuilder : public AppBuilder {
 public:
  MOCK_METHOD0(createReader,
      app::reader::Reader&());
  MOCK_METHOD0(createWriter,
      app::writer::Writer&());
};

}  // namespace core
}  // namespace app
