#ifndef MockIoOutput_H
#define MockIoOutput_H
namespace app {
namespace io {

class MockIoOutput : public IoOutput {
 public:
  MOCK_METHOD1(onDataLine,
      void(std::string data));
};

}  // namespace io
}  // namespace app
#endif
