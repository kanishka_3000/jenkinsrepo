namespace app {
namespace core {

class MockCache : public Cache {
 public:
  MOCK_METHOD1(init,
      void(unsigned int size));
  MOCK_METHOD2(getValue,
      Cache_Result(int index, double& value));
  MOCK_METHOD2(onNewValue,
      void(int index, double value));
  MOCK_METHOD0(debugDump,
      void());
};

}  // namespace core
}  // namespace app
