#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <app/core/Application.h>
#include <TestUtils.h>
#include <app/core/impl/FactoryImpl.h>
class ApplicationTest: public ::testing::Test
{
public:
    void SetUp()
    {

    }
    void TearDown()
    {

    }
protected:
    TestUtils m_utils;
};
TEST_F(ApplicationTest, whenIntegratedTestSimple_thenCacheHit)
{
    m_utils.makeFile("Writer1.txt", {"1 56.66", "7 545.33", "2 566.66623"});
    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);


    m_utils.makeFile("Reader1.txt", {"1", "3", "2", "2","3", "1"});

    m_utils.makeFile("Readers.txt", {"Reader1.txt"});
    m_utils.deleteFile("Reader1.txt.out");


    std::list<double> input;
    m_utils.makeFile("DatFinalDataSet.dat", input);
    app::core::FactoryImpl factory;
    app::core::Application application(factory);

    application.init(1, "Readers.txt", "Writers.txt", "DatFinalDataSet.dat", app::core::MODE::READERS_AFTER_WRITERS);

    std::list<double> output;
    m_utils.getFileData("DatFinalDataSet.dat", output);
    std::vector<double> exp = {56.66, 566.66623, -78.56, -78.56, 3423.663,
                               9090.90, 545.33, 0.0, 9090.90, 3423.663};
    ASSERT_THAT(output, testing::ElementsAreArray(exp));

    std::list<std::string> reader1;


    m_utils.getFileData("Reader1.txt.out", reader1);


    EXPECT_THAT(reader1, testing::ElementsAre("Disk 56.660000", "Disk -78.560000", "Disk 566.666230", "Cache 566.666230","Disk -78.560000", "Disk 56.660000"));
}

TEST_F(ApplicationTest, whenIntegratedTest_thenConditionalValueCheck)
{
    m_utils.makeFile("Writer1.txt", {"1 56.66", "7 545.33", "2 566.66623"});
    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);


    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Reader2.txt", {"6", "1", "7"});
    m_utils.makeFile("Reader3.txt", {"7", "3", "4"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt", "Reader2.txt", "Reader3.txt"});
    m_utils.deleteFile("Reader1.txt.out");
    m_utils.deleteFile("Reader2.txt.out");
    m_utils.deleteFile("Reader3.txt.out");


    std::list<double> input;
    m_utils.makeFile("DatFinalDataSet.dat", input);
    app::core::FactoryImpl factory;
    app::core::Application application(factory);

    application.init(5, "Readers.txt", "Writers.txt", "DatFinalDataSet.dat", app::core::MODE::READERS_AFTER_WRITERS);

    std::list<double> output;
    m_utils.getFileData("DatFinalDataSet.dat", output);
    std::vector<double> exp = {56.66, 566.66623, -78.56, -78.56, 3423.663,
                               9090.90, 545.33, 0.0, 9090.90, 3423.663};
    ASSERT_THAT(output, testing::ElementsAreArray(exp));

    std::list<std::string> reader1;
    std::list<std::string> reader2;
    std::list<std::string> reader3;

    m_utils.getFileData("Reader1.txt.out", reader1);
    m_utils.getFileData("Reader2.txt.out", reader2);
    m_utils.getFileData("Reader3.txt.out", reader3);

    EXPECT_THAT(reader1, testing::ElementsAre(testing::AnyOf("Disk 56.660000", "Cache 56.660000"), testing::AnyOf("Disk -78.560000", "Cache -78.560000"), "Disk 566.666230"));
    EXPECT_THAT(reader2, testing::ElementsAre("Disk 9090.900000", testing::AnyOf("Disk 56.660000", "Cache 56.660000"), testing::AnyOf("Disk 545.330000", "Cache 545.330000")));
    EXPECT_THAT(reader3, testing::ElementsAre(testing::AnyOf("Disk 545.330000", "Cache 545.330000"), testing::AnyOf("Disk -78.560000", "Cache -78.560000"), "Disk -78.560000"));

}

TEST_F(ApplicationTest, whenIntegratedTestConcurrent_thenConditionalValueCheck)
{
    m_utils.makeFile("Writer1.txt", {"1 56.66", "7 545.33", "2 566.66623"});
    m_utils.makeFile("Writer2.txt", {"4 -78.56", "6 9090.90", "5 3423.663"});
    m_utils.makeFile("Writer3.txt", {"3 -78.56", "9 9090.90", "10 3423.663"});
    std::list<string>st = {"Writer1.txt", "Writer2.txt", "Writer3.txt"};
    m_utils.makeFile("Writers.txt", st);


    m_utils.makeFile("Reader1.txt", {"1", "3", "2"});
    m_utils.makeFile("Reader2.txt", {"6", "1", "7"});
    m_utils.makeFile("Reader3.txt", {"7", "3", "4"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt", "Reader2.txt", "Reader3.txt"});
    m_utils.deleteFile("Reader1.txt.out");
    m_utils.deleteFile("Reader2.txt.out");
    m_utils.deleteFile("Reader3.txt.out");

    std::list<double> input;
    m_utils.makeFile("DatFinalDataSet.dat", input);
    app::core::FactoryImpl factory;
    app::core::Application application(factory);

    application.init(5, "Readers.txt", "Writers.txt", "DatFinalDataSet.dat", app::core::MODE::READERS_AFTER_WRITERS);

    std::list<double> output;
    m_utils.getFileData("DatFinalDataSet.dat", output);
    std::vector<double> exp = {56.66, 566.66623, -78.56, -78.56, 3423.663,
                               9090.90, 545.33, 0.0, 9090.90, 3423.663};
    ASSERT_THAT(output, testing::ElementsAreArray(exp));

    std::list<std::string> reader1;
    std::list<std::string> reader2;
    std::list<std::string> reader3;

    m_utils.getFileData("Reader1.txt.out", reader1);
    m_utils.getFileData("Reader2.txt.out", reader2);
    m_utils.getFileData("Reader3.txt.out", reader3);

    EXPECT_THAT(reader1, testing::ElementsAre(testing::AnyOf("Disk 56.660000", "Cache 56.660000"), testing::AnyOf("Disk -78.560000", "Cache -78.560000"), "Disk 566.666230"));
    EXPECT_THAT(reader2, testing::ElementsAre("Disk 9090.900000", testing::AnyOf("Disk 56.660000", "Cache 56.660000"), testing::AnyOf("Disk 545.330000", "Cache 545.330000")));
    EXPECT_THAT(reader3, testing::ElementsAre(testing::AnyOf("Disk 545.330000", "Cache 545.330000"), testing::AnyOf("Disk -78.560000", "Cache -78.560000"), "Disk -78.560000"));

}

TEST_F(ApplicationTest, whenIntegratedProvidedTestConcurrent_thenConditionalValueCheck)
{
     m_utils.makeFile("Writer1.txt", {"1 100", "2 200", "4 300", "5 500", "8 800"});
     std::list<string>st = {"Writer1.txt"};
     m_utils.makeFile("Writers.txt", st);


    m_utils.makeFile("Reader1.txt", {"1", "3", "5", "7", "1", "3", "5", "7"});
    m_utils.makeFile("Readers.txt", {"Reader1.txt"});

     m_utils.makeFile("output.txt", {"0", "23.5", "-33", "", "75.2", "45", "90.0", "100", "8", "9","", "10000.0"});
     app::core::FactoryImpl factory;
     app::core::Application application(factory);
     m_utils.deleteFile("Reader1.txt.out");
     application.init(4, "Readers.txt", "Writers.txt", "output.txt", app::core::MODE::CONCURRENT);

     std::list<std::string> reader1;
     m_utils.getFileData("Reader1.txt.out", reader1);

     EXPECT_THAT(reader1, testing::ElementsAre(testing::AnyOf("Disk 0.000000", "Disk 100.000000"),
                                                "Disk -33.000000",
                                               testing::AnyOf("Disk 75.200000", "Disk 500.000000"),
                                               "Disk 90.000000",
                                               testing::AnyOf("Cache 0.000000", "Cache 100.000000"),
                                               "Cache -33.000000",
                                               testing::AnyOf("Disk 75.200000", "Cache 500.000000", "Cache 75.200000"),
                                               "Cache 90.000000"));
     m_utils.print(reader1);
     std::list<std::string> outputdata;
     m_utils.getFileData("output.txt", outputdata);
     std::vector<std::string> expectout = {"100.000000",
                                           "200.000000",
                                           "-33",
                                           "300.000000",
                                           "500.000000",
                                           "45",
                                           "90.0",
                                           "800.000000",
                                           "8",
                                           "9",
                                           "",
                                           "10000.0"};
     EXPECT_THAT(outputdata, testing::ElementsAreArray(expectout
                     ));
     m_utils.print(outputdata);
     m_utils.deleteFiles({"output.txt", "Reader1.txt.out"});
}

