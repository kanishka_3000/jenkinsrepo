#ifndef TESTUTILS_H
#define TESTUTILS_H
#include <string>
#include <list>
#include <fstream>
using namespace std;

class TestUtils
{
public:
    TestUtils();
    void makeFile(std::string fileName, std::list<double> values);
    void makeFile(std::string fileName, std::list<string> values);

    void getFileData(std::string fileName, std::list<double>& values);
    void getFileData(std::string fileName, std::list<string>& values);

    void deleteFile(std::string fileName);
    void deleteFiles(std::list<std::string> fileNames);

    void print(std::list<string>& values);
};

#endif // TESTUTILS_H
