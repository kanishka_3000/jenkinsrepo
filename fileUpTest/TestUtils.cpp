#include "TestUtils.h"
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;
TestUtils::TestUtils()
{

}

void TestUtils::makeFile(string fileName, std::list<double> values)
{
    std::fstream outputFile;
    outputFile.open(fileName,  std::fstream::out | std::fstream::trunc);

    for(double d : values)
    {
        char zBuff[100];
        snprintf(zBuff, 99, "%f\n", d);
        int len = strlen(zBuff);
        outputFile.write(zBuff, len);
        outputFile.flush();
    }

    outputFile.close();

}

void TestUtils::makeFile(string fileName, std::list<string> values)
{
    std::fstream outputFile;
    outputFile.open(fileName,  std::fstream::out | std::fstream::trunc);

    for(string s : values)
    {
        outputFile.write(s.c_str(), s.length());
        outputFile << endl;
        outputFile.flush();
    }

    outputFile.close();
}

void TestUtils::getFileData(string fileName, std::list<double> &values)
{
    std::fstream outputFile;
    outputFile.open(fileName,  std::fstream::in);
    char zBuff[100];
    while(outputFile.peek() != EOF)
    {
        outputFile.getline(zBuff, 99);
        double d;
        d = atof(zBuff);
        values.push_back(d);
    }
}

void TestUtils::getFileData(string fileName, std::list<string> &values)
{
    std::fstream outputFile;
    outputFile.open(fileName,  std::fstream::in);
    char zBuff[100];
    while(outputFile.peek() != EOF)
    {
        outputFile.getline(zBuff, 99);

        values.push_back(zBuff);
    }
}

void TestUtils::deleteFile(string fileName)
{
    remove(fileName.c_str());
}

void TestUtils::deleteFiles(std::list<string> fileNames)
{
    for(string s : fileNames)
    {
        deleteFile(s);
    }
}

void TestUtils::print(std::list<string> &values)
{
    for(string s : values)
    {
       cout << s.c_str() << endl;
    }
}
